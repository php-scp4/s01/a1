<?php

function getFullAddress($brgy, $city, $province, $country){
    return "$brgy, $city $province, $country";
}

function getLetterGrade($grade) {
    if($grade < 75){
        return "F";
    }else if($grade <= 76){
        return "C-";
    
    }else if($grade <=79){
        return "C";
    
    }else if($grade <=82){
        return "C+";
    
    }else if($grade <=85){
        return "B-";

    }else if($grade <=88){
        return "B";

    }else if($grade <=91){
        return "B+";

    }else if($grade <=94){
        return "A-";

    }else if($grade <=97){
        return "A";

    }else{
        return "A+";
    }
}

?>