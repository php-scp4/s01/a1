<?php require_once './code.php' ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity Code</title>
</head>
<body>
    <!-- ADDRESS -->
    <h1>Full Address</h1>
    <p>
        <?=
         getFullAddress("Sambag 2", "Cebu City", "Cebu", "Philippines");
        ?>
    </p>
    <p>
        <?=
         getFullAddress("Mahaplag", "Baybay City", "Leyte", "Philippines");
        ?>
    </p>
    <!-- GRADING -->
    <h1>LETTER-BASED GRADING</h1>
    <p>87 is equivalent to 
        <?=
         getLetterGrade(87);
        ?>
    </p>
    <p>94 is equivalent to 
        <?=
         getLetterGrade(94);
        ?>
    </p>
    <p>74 is equivalent to 
        <?=
         getLetterGrade(74);
        ?>
    </p>
    <p>76 is equivalent to 
        <?=
         getLetterGrade(76);
        ?>
    </p>

</body>
</html>